# compressible_euler_1d
To simulate one dimensional compressible Euler equations. There are input files for 10-12 test cases input folder and also exact solutions is available in output folder for comparison. There is a python scipt to plot results. 


## Build the code using Cmake

```
- cmake -S . -B build
- cmake --build build

```

## Running the code

```
- cd build
- cd apps
- mkdir output
- ./app ../../input/input_file.txt

```

In the input_file.txt, test cases can be selected

Output will be written to output folder. 

## Plotting results

- copy solution file from the output to the output folder in main directory where Python script resides
- then run the python script as follows 

```
- python3.8 results_plot.py Res_test_case5.txt

```
Just giving the output (result) file name in python script will plot and save the results in pdf format.
